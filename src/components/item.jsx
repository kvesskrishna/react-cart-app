import React, { Component } from "react";
import ITEMS from "./../data/items";
import "./item.css";
class Item extends Component {
  state = {
    // items: ITEMS
  };
  render() {
    return (
      <div className="row item">
        <div className="col-1">
          <img style={{ height: "70px" }} src={this.props.item.thumb} alt="" />
        </div>
        <div className="col-2">
          <span>{this.props.item.title}</span>
        </div>
        <div className="col-2">
          <span>{this.props.item.price}</span>
        </div>
        <div className="col-1">
          <span className="badge badge-success">
            {this.props.item.quantity}
          </span>
        </div>
        <div className="col-1">
          <button
            onClick={this.props.increaseQuantity}
            className="btn btn-success btn-sm"
            disabled={
              this.props.item.quantity == this.props.item.stock ? true : false
            }
          >
            Add
          </button>
        </div>
        <div className="col-1">
          <button
            onClick={this.props.decreaseQuantity}
            className="btn btn-warning btn-sm"
            disabled={this.props.item.quantity == 0 ? true : false}
          >
            Deduct
          </button>
        </div>
        <div className="col-2">
          <button
            onClick={this.props.removeItem}
            className="btn btn-danger btn-sm"
          >
            Remove
          </button>
        </div>
        <div className="col-2">
          {this.props.item.quantity * this.props.item.price}
        </div>
      </div>
    );
  }
}
export default Item;
