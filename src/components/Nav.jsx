import React, { Component } from "react";
class Nav extends Component {
  render() {
    return (
      <div className="row">
        <div className="col">
          <h3>Items in cart: {this.props.cartsize}</h3>
          <h4>Total Units: {this.props.units}</h4>
          <h5>Total Price: ${this.props.cartprice}</h5>
        </div>
        <div className="col">
          {/* <input
            type="text"
            onChange={this.props.searchitems}
            value={this.props.phrase}
            className="form-control"
            placeholder="search"
          /> */}
        </div>
      </div>
    );
  }
  changeHandler = event => {
    this.setState({ phrase: event.target.value });
  };
}

export default Nav;
