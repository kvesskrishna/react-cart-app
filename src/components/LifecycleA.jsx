import React, { Component } from "react";
import LifeCycleB from "./LifecycleB";

class LifeCycleA extends Component {
  constructor() {
    super();
    console.log("Lifecycle A constructor");
  }
  state = { name: "ryan" };
  static getDerivedStateFromProps(props, state) {
    console.log("Lifecycle A getDerivedStateFromProps");
    return null;
  }
  componentDidMount() {
    console.log("Lifecycle A componentDidMount");
  }
  clickHandler = () => {
    this.setState({ name: "krishna" });
  };
  shouldComponentUpdate(nexprops, nextstate) {
    console.log("Lifecycle A shouldComponentUpdate");
    return false;
  }
  getSnapshotBeforeUpdate(prevprops, prevstate) {
    console.log("Lifecycle A getSnapshotBeforeUpdate");
  }
  componentDidUpdate(prevprops, prevstate, snaphsot) {
    console.log("Lifecycle A componentDidUpdate");
  }
  render() {
    console.log("Lifecycle A render");
    return (
      <div>
        Lifecycle A<button onClick={this.clickHandler}>Change Name</button>
        <LifeCycleB />
      </div>
    );
  }
}

export default LifeCycleA;
