import React, { Component } from "react";
import { NumberContext } from "./NumberContext";

class Blue extends Component {
  state = {};
  blue = {
    background: "blue",
    color: "white",
    padding: 20
  };
  render() {
    return (
      <div style={this.blue}>
        <NumberContext.Consumer>
          {context => context.size + context.number}
        </NumberContext.Consumer>
      </div>
    );
  }
}

export default Blue;
