import React, { Component } from "react";
import Green from "./Green";
import { NumberContext } from "./NumberContext";

class Red extends Component {
  state = {
    number: 10,
    size: "large",
    increment: () => {
      this.setState({ number: this.state.number + 1 });
    }
  };
  red = {
    background: "red",
    color: "white",
    padding: 20
  };
  render() {
    return (
      <NumberContext.Provider value={this.state}>
        <div style={this.red}>
          <NumberContext.Consumer>
            {consumer => consumer.number}
          </NumberContext.Consumer>

          <Green />
        </div>
      </NumberContext.Provider>
    );
  }
}

export default Red;
