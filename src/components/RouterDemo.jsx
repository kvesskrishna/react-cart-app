import React, { Component } from "react";
import { BrowserRouter as Router, Route, NavLink } from "react-router-dom";
import Home from "./Home";
import About from "./About";
import Contact from "./Contact";
import User from "./User";

class RouterDemo extends Component {
  state = {};
  render() {
    return (
      <div>
        <Router>
          <ul>
            <li>
              <NavLink exact activeStyle={{ color: "green" }} to="/">
                Home
              </NavLink>
            </li>
            <li>
              <NavLink activeStyle={{ color: "green" }} to="/about">
                About
              </NavLink>
            </li>
            <li>
              <NavLink activeStyle={{ color: "green" }} to="/contact">
                Contact
              </NavLink>
            </li>
            <li>
              <NavLink activeStyle={{ color: "green" }} to="/user">
                User
              </NavLink>
            </li>
          </ul>

          <Route
            exact
            path="/"
            // render={() => {
            //   return <h3>Home Route</h3>;
            // }
            component={Home}
          />
          <Route exact path="/about" component={About} />
          <Route exact path="/contact" component={Contact} />
          <Route exact strict path="/user/:username" component={User} />
        </Router>
      </div>
    );
  }
}

export default RouterDemo;
