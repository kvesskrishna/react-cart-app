import React, { Component } from "react";
import Item from "./item";
import Test from "./test";
import ITEMS from "./../data/items";
import Nav from "./Nav";
import Result from "./result";
class Items extends Component {
  state = {
    itemsarray: ITEMS,
    phrase: ""
  };

  render() {
    let itemsarray =
      this.state.phrase.length == 0
        ? []
        : this.state.itemsarray.filter(item => {
            return item.title.indexOf(this.state.phrase) !== -1;
          });
    let cart = this.state.itemsarray.filter(item => item.quantity > 0);
    let cartsize = cart.length;
    let units = 0;
    let cartprice = 0;
    let message = "Enter a phrase";
    cart.forEach(item => {
      units = units + item.quantity;
      cartprice = cartprice + item.quantity * item.price;
    });
    if (itemsarray.length === 0) {
      if (this.state.phrase.length === 0) {
        message = "Enter a phrase!";
      } else {
        message = "No items found";
      }
    } else {
      message = "";
    }
    return (
      <div>
        <Nav cartsize={cartsize} units={units} cartprice={cartprice} />
        <input
          type="text"
          className="form-control"
          placeholder="Search"
          value={this.state.phrase}
          onChange={this.searchHander}
        />{" "}
        <h2>Results</h2>
        {/* filtered list */}
        <h3>{message}</h3>
        {itemsarray.map(itemobjectinarray => (
          <div>
            <Item
              key={itemobjectinarray.id}
              increaseQuantity={() => this.addHandler(itemobjectinarray.id)}
              removeItem={() => this.removeHandler(itemobjectinarray.id)}
              decreaseQuantity={() =>
                this.decreaseHandler(itemobjectinarray.id)
              }
              item={itemobjectinarray}
            />
          </div>
        ))}
        <h2>Items</h2>
        {/* Actual list */}
        {this.state.itemsarray.map(itemobjectinarray => (
          <div>
            <Result
              key={itemobjectinarray.id}
              increaseQuantity={() => this.addHandler(itemobjectinarray.id)}
              removeItem={() => this.removeHandler(itemobjectinarray.id)}
              decreaseQuantity={() =>
                this.decreaseHandler(itemobjectinarray.id)
              }
              item={itemobjectinarray}
            />
          </div>
        ))}
      </div>
    );
  }
  searchHander = event => {
    // this.setState({ itemsarray: this.state.itemsarray_BAK });
    // const phrase = event.target.value.toLowerCase();
    // let items = [...this.state.itemsarray]; // copy the array from state to local array 'items'
    // console.log(items);
    // let selectedItem = items.filter(item =>
    //   item.title.toLowerCase().includes(phrase)
    // ); // filter the local array to get object with id
    console.log(event.target.value);
    // letph
    this.setState({ phrase: event.target.value });
  };

  addHandler = id => {
    console.log("add btn clicked", id);
    let items = [...this.state.itemsarray]; // copy the array from state to local array 'items'
    let selectedItem = items.filter(item => item.id === id); // filter the local array to get object with id received from child on button click
    console.log(selectedItem);
    const index = items.indexOf(selectedItem[0]);
    selectedItem[0].quantity++; // increment the quantity by 1 on the filtered item
    console.log(
      "stock",
      selectedItem[0].stock,
      "quantity",
      selectedItem[0].quantity
    );

    items[index] = selectedItem[0];
    console.log(items);
    this.setState({ itemsarray: items });
  };
  decreaseHandler = id => {
    console.log("add btn clicked", id);
    let items = [...this.state.itemsarray]; // copy the array from state to local array 'items'
    let selectedItem = items.filter(item => item.id === id); // filter the local array to get object with id received from child on button click
    console.log(selectedItem);
    const index = items.indexOf(selectedItem[0]);
    selectedItem[0].quantity--; // decrement the quantity by 1 on the filtered item
    items[index] = selectedItem[0];
    console.log(items);
    this.setState({ itemsarray: items });
  };
  removeHandler = id => {
    console.log("add btn clicked", id);
    let items = [...this.state.itemsarray]; // copy the array from state to local array 'items'
    let selectedItems = items.filter(item => item.id !== id); // filter the local array to get object with id
    this.setState({ itemsarray: selectedItems });
  };
}

// actual arrray = 1,2,3,4,5
// select all the elements from the array whose index is not 2
// new array = 1,2,4,5

// actual array = new array

export default Items;
