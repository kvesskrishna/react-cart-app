import React, { Component } from "react";
import Blue from "./Blue";
import { NumberContext } from "./NumberContext";

class Green extends Component {
  state = {};
  green = {
    background: "green",
    color: "white",
    padding: 20
  };
  render() {
    return (
      <div style={this.green}>
        {/* <NumberContext.Consumer>{consumer => consumer}</NumberContext.Consumer> */}
        <NumberContext.Consumer>
          {context => <button onClick={context.increment}>Increment</button>}
        </NumberContext.Consumer>
        <Blue />
      </div>
    );
  }
}

export default Green;
