import React, { Component } from "react";
class LifeCycleB extends Component {
  constructor() {
    super();
    console.log("Lifecycle B constructor");
  }
  state = {};
  static getDerivedStateFromProps(props, state) {
    console.log("Lifecycle B getDerivedStateFromProps");
    return null;
  }
  componentDidMount() {
    console.log("Lifecycle B componentDidMount");
  }
  shouldComponentUpdate(nexprops, nextstate) {
    console.log("Lifecycle B shouldComponentUpdate");
    return false;
  }
  getSnapshotBeforeUpdate(prevprops, prevstate) {
    console.log("Lifecycle B getSnapshotBeforeUpdate");
  }
  componentDidUpdate(prevprops, prevstate, snaphsot) {
    console.log("Lifecycle B componentDidUpdate");
  }

  render() {
    console.log("Lifecycle B render");

    return <div>Lifecycle B</div>;
  }
}

export default LifeCycleB;
