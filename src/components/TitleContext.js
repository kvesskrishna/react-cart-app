import React from "react";
const TitleContext = React.createContext("default title");
const TitleProvider = TitleContext.Provider;
const TitleConsumer = TitleContext.Consumer;

export { TitleConsumer, TitleProvider };
export default TitleContext;
