import React, { Component } from "react";
import InputDemo from "./inputdemo";
class FocusInput extends Component {
  constructor() {
    super();
    this.componentRef = React.createRef();
    console.log(this.componentRef);
  }
  state = {};
  clickHandler = () => {
    this.componentRef.current.focusInput();
  };
  render() {
    return (
      <div>
        <InputDemo ref={this.componentRef} />
        <button onClick={this.clickHandler}>Focus Input</button>
      </div>
    );
  }
}

export default FocusInput;
