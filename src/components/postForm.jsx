import React, { Component } from "react";
import Axios from "axios";

class PostForm extends Component {
  state = {
    userId: "",
    title: "",
    body: ""
  };
  render() {
    return (
      <form onSubmit={this.submitPost}>
        <input
          type="text"
          name="userId"
          value={this.state.userId}
          onChange={this.changeHandler}
        />
        <br />
        <input
          type="text"
          name="title"
          value={this.state.title}
          onChange={this.changeHandler}
        />
        <br />
        <input
          type="text"
          name="body"
          value={this.state.body}
          onChange={this.changeHandler}
        />
        <br />
        <button type="submit">Submit</button>
      </form>
    );
  }
  changeHandler = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  submitPost = e => {
    e.preventDefault();
    console.log(this.state);
    Axios.post("https://jsonplaceholder.typicode.com/posts", this.state)
      .then(response => console.log(response))
      .catch(error => console.log(error));
  };
}

export default PostForm;
