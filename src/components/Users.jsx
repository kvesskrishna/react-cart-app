import React, { Component } from "react";
import Axios from "axios";

class Users extends Component {
  state = {
    users: []
  };
  componentDidMount() {
    Axios.get("https://jsonplaceholder.typicode.com/users")
      .then(response => {
        console.log(response);
        this.setState({ users: response.data });
      })
      .catch(error => {
        console.log(error);
      });
  }
  render() {
    return (
      <div>
        <table className="table table-dark">
          <thead>
            <tr>
              <td>Name</td>
              <td>Username</td>
              <td>Email</td>
              <td>Website</td>
            </tr>
          </thead>
          <tbody>
            {this.state.users.map(user => {
              return (
                <tr key={user.id}>
                  <td>{user.name}</td>
                  <td>{user.username}</td>
                  <td>{user.email}</td>
                  <td>{user.website}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

export default Users;
