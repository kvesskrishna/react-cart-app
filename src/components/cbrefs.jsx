import React, { Component } from "react";
class CBRef extends Component {
  constructor() {
    super();
    this.cbref = null;
    this.setcbref = element => {
      this.cbref = element;
    };
  }
  state = {};
  componentDidMount() {
    if (this.cbref) {
      this.cbref.focus();
    }
  }
  render() {
    return (
      <div>
        <input type="text" name="firstname" ref={this.setcbref} />
        <button onClick={this.clickHandler}>Click me</button>
      </div>
    );
  }
  clickHandler = () => {
    let data = {
      field: this.cbref.name,
      value: this.cbref.value
    };
    console.log(data);
  };
}

export default CBRef;
