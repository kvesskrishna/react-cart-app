import React, { Component } from "react";
class InputDemo extends Component {
  constructor() {
    super();
    this.inputRef = React.createRef();
    console.log(this.inputRef);
  }
  state = {};
  focusInput() {
    this.inputRef.current.focus();
  }
  render() {
    return (
      <div>
        <input type="text" ref={this.inputRef} />
      </div>
    );
  }
}

export default InputDemo;
