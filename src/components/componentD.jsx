import React, { Component } from "react";
import { TitleConsumer } from "./TitleContext";

class ComponentD extends Component {
  state = {};
  render() {
    return (
      <TitleConsumer>
        {title => {
          return (
            <div>
              <span>Component D</span>
              <span>The title is {title}</span>
            </div>
          );
        }}
      </TitleConsumer>
    );
  }
}

export default ComponentD;
