import React, { Component } from "react";
class Input extends Component {
  constructor() {
    super();
    this.inputRef = React.createRef();
  }
  state = {};
  componentDidMount() {
    console.log(this.inputRef);
    this.inputRef.current.focus();
  }
  render() {
    return (
      <div>
        <input type="text" name="firstname" ref={this.inputRef} />
        <button onClick={this.clickHandler}>Click me</button>
      </div>
    );
  }
  clickHandler = () => {
    let data = {
      field: this.inputRef.current.name,
      value: this.inputRef.current.value
    };
    console.log(data);
  };
}

export default Input;
