import React, { Component } from "react";
class FragmentDemo extends Component {
  state = {};
  render() {
    return (
      <>
        <h1>Fragment</h1>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda
          laboriosam quidem veritatis. Unde excepturi obcaecati, atque impedit
          distinctio vero, voluptate dicta natus recusandae consequatur deserunt
          quisquam similique ullam soluta! Dolore.
        </p>
      </>
    );
  }
}

export default FragmentDemo;
