const initialState = {
  number: 1
};

const reducer = (state = initialState, action) => {
  const newState = { ...state };
  // related to action
  if (action.type == "ADD") {
    newState.number += 1;
  }
  if (action.type == "SUBSTRACT") {
    newState.number -= 1;
  }
  return newState;
};

export default reducer;
