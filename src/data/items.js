const ITEMS = [
  {
    id: 1,
    title: "Iphone",
    thumb:
      "https://ss7.vzw.com/is/image/VerizonWireless/iphone6s-gld-front?$device-lg$",
    price: "4999.99",
    quantity: 0,
    stock: 10
  },
  {
    id: 2,
    title: "Ipod",
    thumb:
      "https://ss7.vzw.com/is/image/VerizonWireless/iphone6s-gld-front?$device-lg$",
    price: "500",
    quantity: 0,
    stock: 12
  },
  {
    id: 3,
    title: "Shoes",
    thumb:
      "https://assets.reebok.com/images/w_600,f_auto,q_auto:sensitive,fl_lossy/4674dd20907f4ad69fd3a9ab015d4976_9366/Club_C_85_Women's_Shoes_White_BS7685_01_standard.jpg",
    price: "5000",
    quantity: 0,
    stock: 15
  }
];
export default ITEMS;
