import React, { Component } from "react";
import ComponentD from "./components/componentD";
import ComponentE from "./componentE";
class ComponentC extends Component {
  state = {};
  render() {
    return (
      <div>
        <ComponentD />
        <ComponentE />
      </div>
    );
  }
}

export default ComponentC;
