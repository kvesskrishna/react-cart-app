import React, { Component } from "react";
import TitleContext from "./components/TitleContext";
class ComponentE extends Component {
  state = {};
  render() {
    return <div>Component E context is {this.context}</div>;
  }
}
ComponentE.contextType = TitleContext;
export default ComponentE;
