// import React from "react";
// import ReactDOM from "react-dom";
// import "./index.css";
// import App from "./App";
// import * as serviceWorker from "./serviceWorker";
// import { Provider } from "react-redux";
// import { createStore } from "redux";
// import reducer from "./store/reducer";

// const store = createStore(reducer);

// ReactDOM.render(
//   <Provider store={store}>
//     <App />
//   </Provider>,
//   document.getElementById("root")
// );

// serviceWorker.unregister();

import { createStore } from "redux";

const initialstate = {
  result: 1,
  lastValues: []
};

const reducer = (state = initialstate, action) => {
  switch (action.type) {
    case "ADD":
      // state.result += action.payload;
      state = {
        ...state,
        result: state.result + action.payload,
        lastValues: [...state.lastValues, action.payload]
      };
      break;
    case "SUBSTRACT":
      state = {
        ...state,
        result: state.result - action.payload,
        lastValues: [...state.lastValues, action.payload]
      };
      break;
  }
  return state;
};

const store = createStore(reducer);

store.subscribe(() => console.log("store updated", store.getState()));

store.dispatch({ type: "ADD", payload: 10 }); //11
store.dispatch({ type: "ADD", payload: 20 }); //31
store.dispatch({ type: "SUBSTRACT", payload: 10 }); //21
