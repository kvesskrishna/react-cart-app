import React, { Component } from "react";
import Items from "./components/items";
import LifeCycleA from "./components/LifecycleA";
import FragmentDemo from "./components/FragmentDemo";
import Input from "./components/input";
import CBRef from "./components/cbrefs";
import FocusInput from "./components/focusinput";
import ComponentD from "./components/componentD";
import { TitleProvider } from "./components/TitleContext";
import ComponentC from "./componentC";
import Users from "./components/Users";
import PostForm from "./components/postForm";
import Red from "./components/Red";
import RouterDemo from "./components/RouterDemo";
import { Fragment } from "react";
import { connect } from "react-redux";

class App extends Component {
  // state = { number: 1 };
  // onIncrease = () => {
  //   this.setState({ number: this.state.number + 1 });
  // };
  // onDecrease = () => {
  //   this.setState({ number: this.state.number - 1 });
  // };
  render() {
    return (
      <Fragment>
        <div>
          Number: <span>{this.props.number}</span>
        </div>
        <button onClick={this.props.onIncrease}>Inc. Number</button>
        <button onClick={this.props.onDecrease}>Dec. Number</button>
      </Fragment>
    );
    // <RouterDemo />;
    // <Red />;
    // <PostForm />;
    // <Users />;
    // (
    //   <TitleProvider value="context demo">
    //     <ComponentC />
    //   </TitleProvider>
    // );
    // <FocusInput />;
    // <CBRef />;
    // <Input />;
    // <FragmentDemo />;
    // <LifeCycleA />;
    // <Items />
  }
}
const mapDispatchToProps = dispatch => {
  return {
    onIncrease: () => dispatch({ type: "ADD" }), // store.dispatch()
    onDecrease: () => dispatch({ type: "SUBSTRACT" })
  };
};
const mapStateToProps = state => {
  return {
    number: state.number
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);

// export default App;
